This repo contains solution to Parking Lot task

To run it as simple python script, firstly provide your version of `data/parking_data.json` file,  and then run in this directory. Also, you should install some missing dependencies using pip (python package manager)

```bash
pip install numpy matplotlib
```
After that, run script using:

```bash
python parking_lot.py
```

If you are interested only in algorithmh, check pls `notebooks/parking_lot.html`. This file contains code, and description to approach.
